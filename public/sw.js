const CACHE_NAME = "site-cache-v1";
const CACHE_THIS = [
  "/",
  "/index.html",
  "/manifest.json",
  "/styles/style.css",
  "/styles/toast.css",
  "/js/main.js",
  "/js/app.js",
  "/assets/svg/eye.svg",
  "/assets/icons/icon-192x192.png",
  "/assets/icons/icon-256x256.png",
  "/assets/icons/icon-384x384.png",
  "/assets/icons/icon-512x512.png",
];
self.addEventListener("install", (event) => {
  // console.log("Service worker has been installed", event);
  event.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      console.log("Caching files...");
      cache.addAll(CACHE_THIS).then(() => {
        console.log("Caching complete");
      });
    })
  );
});

self.addEventListener("activate", (event) => {
  // console.log("Service worker has been activated", event);
  event.waitUntil(
    caches.keys().then((keys) => {
      // console.log(keys);
      return Promise.all(
        keys
          .filter((key) => key !== CACHE_NAME)
          .map((key) => {
            caches.delete(key);
          })
      );
    })
  );
});

self.addEventListener("fetch", (event) => {
  // console.log("Service worker if fetching", event);
  event.respondWith(
    caches.match(event.request).then((cacheResponse) => {
      return cacheResponse || fetch(event.request);
    })
  );
});
