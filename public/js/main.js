const color1 = document.getElementById("color-1");
const color2 = document.getElementById("color-2");
const degDisplay = document.getElementById("degDisplay");
const container = document.querySelector(".container");
var csscode = document.getElementById("code");
csscode.value =
  "background-image: linear-gradient(to bottom, #1488cc, #2b32b2);";
function setDeg(_this) {
  degDisplay.innerHTML = _this.value + "°";
  changegrd(_this.value);
}

function changegrd(degValue) {
  csscode.value = `background-image: linear-gradient(${degValue}deg, ${color1.value}, ${color2.value});`;
  document.body.style.backgroundImage = `linear-gradient( ${degValue}deg, ${color1.value}, ${color2.value})`;
}

function copycss() {
  csscode.select();
  document.execCommand("copy");
  window.getSelection().removeAllRanges();
  showToast();
}

function showToast() {
  var x = document.getElementById("toast");
  x.className = "show";
  setTimeout(function () {
    x.className = x.className.replace("show", "");
  }, 3000);
}

function fullscreen() {
  container.style.display = "none";
  let flex = setInterval(() => {
    container.style.display = "flex";
    clearInterval(flex);
  }, 1250);
}
